from flask import jsonify

def model_valid(fields = dict):
    is_error = False
    errors_list = []
    sentense = None
    for key , value in fields.items():
        if value == None:
            sentense = f"The {key} field is required"
            errors_list.append(sentense)
            is_error = True

    return is_error , errors_list