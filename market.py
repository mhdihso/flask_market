from flask import Flask , render_template
from flask_sqlalchemy import SQLAlchemy
from flask import jsonify
from flask import request
from model_validetors import model_valid

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///market.db'
db = SQLAlchemy(app)

class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=50), nullable=False,unique=True)
    price = db.Column(db.Integer(), nullable=False)
    barcode = db.Column(db.String(length=50), nullable=False, unique = True)
    description = db.Column(db.String(length=128) , nullable=True)

    @property
    def serialize(self):
        return {
            'id' : self.id,
            'name': self.name,
            'price'  : self.price,
            'barcode' : self.barcode,
            "description" : self.description,
        }


    def __repr__(self):
        return f'Item {self.name}'


@app.route('/market/list/',methods = ['GET', 'POST'])
def market_list():
    if request.method == 'GET':
        page = request.args.get("page", 1, type=int)
        per_page = request.args.get("per-page", 100, type=int)
        items = Item.query.paginate(page,per_page,error_out=False)
        pagination_data = {
            "count": items.total,
            "page": page,
            "per_page": per_page,
            "pages": items.pages,

        }
        items_ = items.items
        return jsonify(pagination_data = pagination_data,result=[i.serialize for i in items_])
    else:
        name = request.get_json().get('name' , None)
        price = request.get_json().get('price' , None)
        barcode = request.get_json().get('barcode' , None)
        description = request.get_json().get('description' , None)

        fields = {
            "name"  : name,
            "price" : price,
            "barcode" : barcode
        }
        is_error , errors_list = model_valid(fields = fields)
        if is_error:
            return jsonify(detail = errors_list)

        item = Item(name = name , price = price , barcode = barcode  , description = description )
        db.session.add(item)
        db.session.commit()
        return jsonify(result=item.serialize)

@app.route('/market/<int:id>/',methods = ['DELETE', 'PUT' , 'PATCH' , 'GET'])
def market_detail(id):
    item = Item.query.get(id)
    if request.method == 'DELETE':
        db.session.delete(item)
        db.session.commit()
        return jsonify() , 204
    elif request.method == 'PUT' or request.method == 'PATCH': 
        name = request.get_json().get('name' , None)
        price = request.get_json().get('price' , None)
        barcode = request.get_json().get('barcode' , None)
        description = request.get_json().get('description' , None)
        fields = {
            "name"  : name,
            "price" : price,
            "barcode" : barcode,
            "description" : description,
        }
        if request.method == 'PATCH':
            pass
        else:
            is_error , errors_list = model_valid(fields = fields)
            if is_error:
                return jsonify(detail = errors_list)
    
        for field, value in fields.items():
            code = f"item.{field} = {field}" if value is not None else None
            exec(code) if value is not None else None

        return jsonify(result=item.serialize), 200

    else:
        return jsonify(result=item.serialize)